import sys
import subprocess
sys.path.append('/home/jetson/sauron')
import os
import logging
from datetime import datetime, date, timedelta
from config import Config

cfg = Config()
logging.basicConfig(filename=os.path.join(cfg.TODAY_DIR, "log.log"), level=logging.INFO)
flag = True
try:
    import board
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "board": ' + str(e))
    flag = False

try:
    import digitalio
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "digitalio": ' + str(e))
    flag = False
if flag:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Import sudo libs successful')
else:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Import sudo libs fail')
    subprocess.check_call('/usr/bin/python3 /home/jetson/sauron/screens.py 2', shell=True)