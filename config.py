import os
import csv 
from datetime import datetime
import numpy as np

class Config:
#dir
    CURR_DIR = '/home/jetson/sauron'
    SRC_DIR = os.path.join(CURR_DIR, 'sources')
    WEIGHTS_DIR = os.path.join(SRC_DIR, 'weight')
    PRODUCT_IMG_DIR = os.path.join(SRC_DIR, 'image', 'product')
    CATEGORY_IMG_DIR = os.path.join(SRC_DIR, 'image', 'category')
    SYSTEM_IMG_DIR = os.path.join(SRC_DIR, 'image', 'system')
    DATABASE_DIR = os.path.join(SRC_DIR, 'database')
    GPIO_DIR = os.path.join(CURR_DIR, 'periphery')
    NN_DIR = os.path.join(CURR_DIR, 'NN')
    SMARKET_DIR = os.path.join(CURR_DIR, 'smarket')
    dt = datetime.now()
    dt = dt.strftime("%d_%m_%Y")
    TODAY_DIR = os.path.join(CURR_DIR, 'data', dt)
    if not os.path.exists(TODAY_DIR):
        os.makedirs(TODAY_DIR)
    XML = os.path.join(TODAY_DIR, 'stat.txt')
#--dir

#periphery(cam, ttl, duino)
    CAM_TYPE = 'USB'
    CAM_ADDR = '/dev/video0'
    WH = (1920, 1080)
    
    BAUDRATE = 9600
    TICKS_FALL_SCALE = 20    

    BUS_INDEX = 0
    SLAVE_ADRESS = 0x40
#--periphery(cam, ttl, duino)

#NN
    ONNX_WEIGHTS = os.path.join(WEIGHTS_DIR,'ModelCam_50_p.onnx')
    TH = 0.0001

    CLASSES = []
    ALPHABET = ['','','','','','']
    DELETE_CLASSES = []
    with open(os.path.join(DATABASE_DIR, 'NN_classes.csv'), encoding="utf-8") as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for i, row in enumerate(spamreader):
            if row[0] != '':
                CLASSES.append(row[0].upper())

    with open(os.path.join(DATABASE_DIR, 'delete_classes.csv'), encoding="utf-8") as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        for i, row in enumerate(spamreader):
            if row[0] != '':
                DELETE_CLASSES.append(row[0].upper())

    for el in CLASSES:
        if not el in DELETE_CLASSES:
            if el != 'bg' and el !='BG' and el != '':
                ALPHABET.append(el)
    ALPHABET.append('НЕТ КАТЕГОРИИ')


    CLASSES.sort()
    ALPHABET.sort()

#interface
    Y_INIT_PRODUCTS = 18    

    X_LEFT_ARROW= 0.7429420505

    WIDTH_ICONS = 22.28826152
    HEIGHT_ICONS = 35

    WIDTH_ARROW = 9.286775632
    HEIGHT_ARROW = 38.4 

    WIDTH_SPACE = 3.268945022
    HEIGHT_SPACE = 6.861155556

    HEIGHT_ICONS_IMG = 8.749713241
    
    HEIGHT_OFFSET = 1.382882883
    WIDTH_OFFSET = 2.7242582646
#--interface

#wathcdog
    WATCHDOG_SOCKET = '1957'
    WATHCDOG_LOG = os.path.join(TODAY_DIR, 'watchdog.log')
#--wathcdog
    
#logic
    THRESHOLD_BG = 0.5
    THRESHOLD_TIMER_NOT_BG_NOT_BG = 15 #при тестировании поставить 1, при работе 15
    THRESHOLD_TIMER_NOT_BG_BG = 2 
    THRESHOLD_TIMER_BG_BG = 3 #при тестированиии поставить 0.5, при работе 7
    THRESHOLD_TIMER_SCALE = 2
#--logic

#gpio
    COUNT_SHORT_CLICK = 3
    MIN_TIME_FOR_CALIBR = 1
    MAX_TIME_FOR_CALIBR = 3
    MIN_TIME_FOR_BACK = 5
    MAX_TIME_FOR_BACK = 10
    MIN_TIME_FOR_REBOOT = 12
    MAX_TIME_FOR_REBOOT = 100
#--gpio

    #DEVs = ['Virtualcorepointer', 'VirtualcoreXTESTpointer', 'ByQDtechUSBid6slavepointer2', 'Virtualcorekeyboard', 'VirtualcoreXTESTkeyboard', 'gpiokeys', 'USB20Camera']
    DEVs = ['Virtualcorepointer', 'VirtualcoreXTESTpointer', '深圳市全动电子技术有限公司ByQDtech触控USB鼠标\tid=6\t[slavepointer(2)', 'Virtualcorekeyboard', 'VirtualcoreXTESTkeyboard', 'USB2.0Camera', 'gpio-keys'] 
