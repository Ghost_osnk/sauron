import sys
sys.path.append('/home/jetson/sauron')
import cv2
import time
import subprocess
import logging
from config import Config


cfg = Config()
cap = cv2.VideoCapture(0)
cv2.namedWindow('calib img', cv2.WND_PROP_FULLSCREEN)
cv2.resizeWindow('calib img', 800, 600)
cv2.setWindowProperty('calib img', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
cap.set(3, cfg.WH[0])
cap.set(4, cfg.WH[1])
index = 0
cam_index = 0
while True:
    r,f = cap.read()
    if f is not None:
        try:
            (h,w) = f.shape[:2]
            center = (w/2, h/2)
            cv2.imshow('calib img', f)
        except:
            pass
    else:
        cap = cv2.VideoCapture(index)
        if index == 20:
            cap_index += 1
            if cap_index == 5:
                try:
                    subprocess.check_call('reboot')
                except:
                    subprocess.check_call(['systemctl', 'reboot', '-i'])
            cap = cv2.VideoCapture(cap_index)
        else:
            continue
    cv2.waitKey(1)

