import json, base64, hashlib
import requests

# @author: barbass1025@gmail.com
# Класс для работы с бэкендом Revotail (обмен списком товаров, категорий)
class RevotailServerApi:
    url = ''
    device_id = ''
    password = ''
    methods = {
        'category_list': 'category_list',
        'product_list': 'product_list',
        'save_category_list': 'save_category_list',
        'save_product_list': 'save_product_list',
    }

    def __init__(self, url, device_id, password):
        self.url = url
        self.device_id = device_id
        self.password = password

    # Создание подписи для запроса
    def create_sign_to_request(self, data):
        # Set indent = 0, because PHP and Python differently encode
        data_json = json.dumps(data, separators=(',', ':'))
        # Because limit in server to length data
        data_json = data_json[0:40]

        sign = hashlib.pbkdf2_hmac(
            hash_name='sha512', 
            password=(data_json.encode('utf-8')),
            salt=self.password.encode(),
            iterations=1000, 
            dklen=40
        )

        return sign

    # Проверка подписи ответа
    def check_sign_from_response(self, data, sign_response):
        # Set indent = 0, because PHP and Python differently encode
        data_json = json.dumps(data, separators=(',', ':'))
        data_json = data_json[0:40]

        sign = hashlib.pbkdf2_hmac(
            hash_name='sha512', 
            password=(data_json.encode('utf-8')),
            salt=self.password.encode(),
            iterations=1000, 
            dklen=40
        )

        return sign == sign_response

    # Формирование данных для запроса
    def construct_send_data(self, in_data=None):
        if in_data != None:
            data = in_data
        else:
            data = [] # Because server-php love is array
        
        sign = self.create_sign_to_request(data)
        
        send_dict = {
            'data': data,
            'device_id': self.device_id,
            'sign': base64.b64encode(sign).decode()
        }

        return send_dict

    # Проверка ответа сервера на валидность
    def check_response(self, response):
        if response.status_code != 200:
            raise Exception('Response ' + str(response.status_code))
        if response.headers['Content-Type'].find('application/json') == -1:
            raise Exception('Content-Type ' + response.headers['Content-Type'])

        result = None

        try: 
            result = response.json()
        except Exception as e:
            raise e

        if result.get('sign') == None:
            raise Exception('Not found sign')

        if result.get('data') == None:
            raise Exception('Not found data')

        return result

    # Метод: Получение списка категорий
    def get_category_list(self):
        try:
            response = requests.post(
                self.url + self.methods['category_list'],
                json=self.construct_send_data()
            )

            return self.check_response(response)
        except Exception as e:
            raise e

    # Метод: Получение списка товаров
    def get_product_list(self):
        try:
            response = requests.post(
                self.url + self.methods['product_list'],
                json=self.construct_send_data()
            )

            return self.check_response(response)
        except Exception as e:
            raise e

    # Метод: Сохранение списка категорий
    def save_category_list(self, category_list):
        try:
            response = requests.post(
                self.url + self.methods['save_category_list'],
                json=self.construct_send_data({'category_list':category_list})
            )

            return self.check_response(response)
        except Exception as e:
            raise e

    # Метод: Сохранение списка товаров
    def save_product_list(self, product_list):
        try:
            response = requests.post(
                self.url + self.methods['save_product_list'],
                json=self.construct_send_data({'product_list':product_list})
            )

            return self.check_response(response)
        except Exception as e:
            raise e
