import os
import zmq
import sys
sys.path.append('/home/jetson/sauron')
from threading import Thread
from config import Config
import time
import board
import digitalio
import subprocess
import argparse

class Worker:

    def init_state(self, flag):
        self.cfg = Config()
        os.chdir(self.cfg.GPIO_DIR)
        if flag == '0':
            self.calibrate_flag = False
            context = zmq.Context()
            self.socket = context.socket(zmq.PUSH)
            self.socket.connect("tcp://localhost:" + self.cfg.WATCHDOG_SOCKET)
            socket_th = Thread(target=self.watchdog_send)
            socket_th.start()
        else:
            self.calibrate_flag = True

        self.first_flag = True
        self.click_flag = False
        self.count_short_click = self.cfg.COUNT_SHORT_CLICK
        self.min_time_for_calibr = self.cfg.MIN_TIME_FOR_CALIBR
        self.max_time_for_calibr = self.cfg.MAX_TIME_FOR_CALIBR
        self.min_time_for_reboot = self.cfg.MIN_TIME_FOR_REBOOT
        self.max_time_for_reboot = self.cfg.MAX_TIME_FOR_REBOOT

        diod = digitalio.DigitalInOut(board.D19)
        diod.direction = digitalio.Direction.OUTPUT
        diod.value = False

        button = digitalio.DigitalInOut(board.D18)
        button.direction = digitalio.Direction.OUTPUT
        button.value = False

        st_time = time.time()
        en_time = time.time()

        index = 0
        self.main_work(diod, button, st_time, en_time, index, flag)

    def main_work(self, diod, button, st_time, en_time, index, flag):
        while True:
            button.direction = digitalio.Direction.INPUT
            button_click = button.value
            if button_click:
                if not self.click_flag:
                    self.click_flag = True
                    if self.first_flag:
                        self.first_flag = False
                        st_time = time.time()
                        continue
                    en_time = time.time()
                    index, result = self.check(st_time, en_time, index, button_click)
                    if not result:
                        break
                    else:
                        st_time = time.time()
            else:
                if self.click_flag:
                    self.click_flag = False
                    if self.first_flag:
                        continue
                    en_time = time.time()
                    index, result = self.check(st_time, en_time, index, button_click)
                    if not result:
                        break
                    else:
                        st_time = time.time()
            button.direction = digitalio.Direction.OUTPUT
            button.value = False
            time.sleep(0.1)

        diod.value = True
        time.sleep(2)
        diod.direction = digitalio.Direction.INPUT
        diod.direction = digitalio.Direction.OUTPUT
        diod.value = False
        self.init_state(flag)

    def watchdog_send(self):
        while True:
            self.socket.send_string('gpio_func')
            time.sleep(55)

    def check(self, st_time, en_time, index, button_click):
        result = en_time - st_time
        if not self.calibrate_flag:
            if (button_click and index%2 == 1) or (not button_click and index%2 == 0): 
                if index <= self.count_short_click * 2 - 1:
                    if result <= 1.2:
                        index += 1
                        return index, True
                    else:
                        return index, False
                else:
                    if self.min_time_for_calibr <= result <= self.max_time_for_calibr:
                        self.socket.send_string('calibrate')
                        subprocess.call('./periphery_calibrate.sh')
                        return 0, True
                    elif self.min_time_for_reboot <= result <= self.max_time_for_reboot:
                        try:
                            subprocess.check_call('reboot')
                        except:
                            subprocess.check_call(['systemctl', 'reboot', '-i'])
                    else:
                        return index, False
        else:
            if result <= 5:
                subprocess.call('./periphery_next_state.sh')
                return 0, False
            elif result <= 10:
                try:
                    subprocess.check_call('reboot')
                except:
                    subprocess.check_call(['systemctl', 'reboot', '-i'])
                return 0, False
            else:
                return 0, False

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('flag', help='init state calibrate flag')
    args = parser.parse_args()
    wrk = Worker()
    wrk.init_state(args.flag)
