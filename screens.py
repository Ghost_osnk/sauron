import sys
import tkinter as tk
from tkinter import *
import os
from config import Config
from PIL import Image
from PIL import ImageTk
import argparse
from threading import Thread
import time

def Timer():
    start = time.time()
    while True:
        if time.time()-start > 600:
            os.abort()

parser = argparse.ArgumentParser()
parser.add_argument('mode', default=0, nargs='?')
args = parser.parse_args()
mode = int(args.mode)
if mode == 0:
    thread_timer = Thread(target=Timer, args=())
    thread_timer.start()

try:
    root = tk.Tk()
    root.geometry(1024, 600)
except Exception as e:
    print('screens error:',str(e))
cfg = Config()
root.wm_attributes('-fullscreen','true')
root.configure(bg="white")
path = cfg.SYSTEM_IMG_DIR
if mode == 0:
    path = os.path.join(path, 'init_load.png')
elif mode == 1:
    path = os.path.join(path, 'tech_work.png')
elif mode == 2:
    path = os.path.join(path, 'cam_fall.png')
elif mode == 3:
    path = os.path.join(path, 'ttl_fall.png')
elif mode == 4:
    path = os.path.join(path, 'digi_fall.png')
img = Image.open(path, 'r')
img = img.resize((root.winfo_screenwidth(), root.winfo_screenheight()), Image.ANTIALIAS)
img = ImageTk.PhotoImage(img)
panel1 = tk.Label(root, image=img)
panel1.pack(side='top', fill='both', expand='yes')
root.mainloop()

