import sys
sys.path.append('/home/jetson/sauron')
import os
import cv2
import time
import serial
import logging
import subprocess
from config import Config
from datetime import datetime, date, timedelta

cfg = Config()
logging.basicConfig(filename=os.path.join(cfg.TODAY_DIR, "log.log"), level=logging.INFO)

leo = ''.join(os.popen('i2cdetect -y -r 0', 'r', 1).read().split('\n'))

if leo.find('40: 40 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --') != -1:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Arduino detected. Adress = 0x40')
else:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' No arduino detected')
    subprocess.check_call('/home/jetson/sauron/screens_script.sh 4', shell=True)

cam = ''.join(os.popen('ls /dev/video*', 'r', 1).read().split('\n'))
if cam.find('/dev/video') != -1:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Camera detected. Adress = ' + cam)
else:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' No camera detected')

scale = ''.join(os.popen('ls /dev/ttyUSB*', 'r', 1).read().split('\n'))
if scale.find('/dev/ttyUSB') != -1:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Scale detected. Adress = ' + scale)
else:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' No scale detected')

cap = cv2.VideoCapture(cam)
index_cam = 0
for i in range (10):
    r,f = cap.read()
    if f is not None:
        index_cam += 1
if index_cam != 0:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Camera work')
else:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + " Couldn't get the camera image")
    subprocess.check_call('/home/jetson/sauron/screens_script.sh 2', shell=True)

ser = serial.Serial(scale, 9600, timeout=3)
print(scale)
flag = True
for i in range (10):
    w = ser.read_until(b'\x00')
    print('w', w)
    if len(w) == 0:
        flag = False
        break
    time.sleep(0.5)
if flag:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Scale work')
else:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + " Couldn't get the scale weight")
    subprocess.check_call('/home/jetson/sauron/screens_script.sh 3', shell=True)
logging.info('--------------------')
