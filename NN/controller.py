import os
import numpy as np
import zmq
from time import time, sleep
from datetime import datetime
from threading import Thread
from subprocess import PIPE, Popen, STDOUT
import sys
import json
import gc
sys.path.append('../')
sys.path.append('.')
try:
    from config import Config
except:
    from default_config import Config


def sm_sting_to_numpy(sm_string):
    vals_str = sm_string.split(',')
    vals_float = list(map(float, vals_str[:-1]))
    vals = np.array(vals_float)
    return vals


class Controller():
    def __init__(self, debug=False):
        if not debug:
            self.cfg = Config
            print(len(self.cfg.CLASSES))
            with open(os.path.join(self.cfg.NN_DIR, 'meta_srn.json'), 'r') as fp:
                meta = json.load(fp)
                roi = ','.join(list(map(str, np.array(meta['roi'], dtype=np.int))))
            self.is_cam_online = True
            
            cmd = ['./trt_camera_inference', 
                   self.cfg.CAM_ADDR, 
                   self.cfg.ONNX_WEIGHTS, 
                   str(len(self.cfg.CLASSES)), 
                   roi, 
                   ','.join(list(map(str, self.cfg.WH))), 
                   str(self.cfg.TH),
                   self.cfg.TODAY_DIR,
                   "NOT_SAVE"]

            print('Command ', ' '.join(cmd))
            cmd = ' '.join(cmd)
            self.p = Popen(cmd, stdout=PIPE, stderr=STDOUT, shell=True, cwd=self.cfg.NN_DIR)

            self.t_log = Thread(target=self.log_f)
            self.t_log.daemon = True
            self.t_log.start()

            

        context = zmq.Context()
        self.socket = context.socket(zmq.PAIR)
        self.socket.connect("tcp://localhost:1488")
        self.topS = ([], [])
        
        if not debug:
            print('Waiting READY signal from TRT')
            zqm_mess = self.socket.recv()
            print('TRT says:', zqm_mess.decode())

            self.started = True
            self.t = Thread(target=self.listen)
            self.t.daemon = True
            self.t.start()
    
    def log_f(self):
        # ALSO CANCER
        for stdout_line in iter(self.p.stdout.readline, ""):
            if stdout_line.decode() != '':
                print('CPP LOG:', stdout_line.decode().rstrip())
        self.p.stdout.close()
        self.p.wait()

    def listen(self):
        while self.started:
            # Basicaly nothing breaks watiting CANCER
            sm_string = self.socket.recv().decode()
            cmd, data= sm_string.split(' ')
            print(sm_string)
            if cmd == 'SMP':
                sm = sm_sting_to_numpy(data)

                idxes = np.argsort(sm)[::-1]

                top = idxes[:]
                top_names = []
                top_scores = []
                print(idxes.shape)
                print(idxes)
                for t in top:
                    top_names.append(self.cfg.CLASSES[t])
                    top_scores.append(sm[t])
                self.topS = (top_names, top_scores)
            elif cmd == 'CMF':
                print('CAMERA FAIL')
                self.is_cam_online = False

    def is_camera_online(self):
        return self.is_cam_online

    def stop(self):
        self.started = False
        self.socket.send_string('STP')
        self.t.join()

    def get_top(self):
        retval = [self.topS[0][:], self.topS[1][:]]
        return retval

    def save_img(self, gt=None):
        dt = datetime.now()
        sn = '{:02d}{:02d}{:02d}{:02d}{:02d}.jpg'.format(dt.day, dt.month, dt.hour, dt.minute, dt.second)
        date_time = datetime.now().strftime("%d_%m_%Y")
        curr_dir = self.cfg.TODAY_DIR
        if not os.path.exists(curr_dir):
            os.makedirs(curr_dir)
        self.socket.send_string('SVI {}'.format(os.path.join(curr_dir, sn)))


def main():
    cntr = Controller()
    while True:
        sleep(0.5)
        # cntr.save_img()
        if not cntr.is_camera_online():
            print('Camera fault')
            break
        print(cntr.get_top())
    cntr.stop()

if __name__ == '__main__':
    main()


