import sys
sys.path.append('/home/jetson/sauron')
import time
import board
import digitalio
import threading
import subprocess
import os
import zmq
from config import Config

class Worker:

    def init_state(self):
        self.cfg = Config()
        os.chdir(self.cfg.GPIO_DIR)
        self.button_cl = 0
        context = zmq.Context()
        self.socket = context.socket(zmq.PAIR)
        self.socket.bind("tcp://*:2419")
        self.first_flag = True
        self.click_flag = False
        self.exit_flag = False

        self.min_time_for_calibr = self.cfg.MIN_TIME_FOR_CALIBR
        self.max_time_for_calibr = self.cfg.MAX_TIME_FOR_CALIBR
        self.min_time_for_back = self.cfg.MIN_TIME_FOR_BACK
        self.max_time_for_back = self.cfg.MAX_TIME_FOR_BACK
        self.min_time_for_reboot = self.cfg.MIN_TIME_FOR_REBOOT
        self.max_time_for_reboot = self.cfg.MAX_TIME_FOR_REBOOT

        diod = digitalio.DigitalInOut(board.D19)
        diod.direction = digitalio.Direction.OUTPUT
        diod.value = False

        button = digitalio.DigitalInOut(board.D18)
        button.direction = digitalio.Direction.OUTPUT
        button.value = False

        st_time = time.time()
        en_time = time.time()
        self.main_work(diod, button, st_time, en_time) 

    def main_work(self, diod, button, st_time, en_time):
        button.direction = digitalio.Direction.INPUT
        button_click = button.value
        st_time = time.time()
        result = True
        while not self.exit_flag:
            button.direction = digitalio.Direction.INPUT
            button_click = button.value
            if button_click:
                if not self.click_flag:
                    self.click_flag = True
                    if self.first_flag:
                        self.first_flag = False
                        st_time = time.time()                    
            else:
                if self.click_flag:
                    self.click_flag = False
                    if self.first_flag:
                        continue
                    en_time = time.time()
                    result = self.check_timer(st_time, en_time, button_click)       
                    if not result:
                        result = True
                        break            
            button.direction = digitalio.Direction.OUTPUT
            button.value = False
            time.sleep(0.1)
        if result:
            if self.socket.recv().decode() == 'reboot':
                time.sleep(3)
                try:
                    subprocess.check_call('reboot')
                except:
                    subprocess.check_call(['systemctl', 'reboot', '-i'])

        diod.value = True
        time.sleep(2)
        diod.direction = digitalio.Direction.INPUT
        diod.direction = digitalio.Direction.OUTPUT
        diod.value = False
        self.init_state()

    def check_timer(self, st_time, en_time, button_click):
            result = en_time - st_time  
            if self.min_time_for_calibr < result < self.max_time_for_calibr:
                self.socket.send_string('save')
                self.exit_flag = True
            elif self.min_time_for_back < result < self.max_time_for_back:
                self.socket.send_string('back')
                self.exit_flag = True
                time.sleep(1.5)
                subprocess.call('/home/jetson/sauron_8/periphery/periphery_calibrate.sh')
            elif self.min_time_for_reboot < result < self.max_time_for_reboot:
                self.socket.send_string('reboot')
                self.exit_flag = True
                time.sleep(1.5)
            else:
                return False

if __name__ == '__main__':
    wrk = Worker()
    wrk.init_state()
