import psutil
import logging
from datetime import datetime, date, timedelta
from threading import Thread
import time
import subprocess
import os
from config import Config
import zmq
#state_proc: 0 - C++, 1 - interfaces, 2 - gpio
#state_per: 0 - cam, 1 - leo, 2 - ttl
class WatchDog():
    def __init__(self):
        self.cfg = Config()
        self.max_proc = {'time':None, 'value':float(0)}
        self.max_mem = {'time':None, 'value':float(0)}
        self.max_swap_mem = {'time':None, 'value':float(0)}
        self.flag_calibrate = False
        logging.basicConfig(filename=self.cfg.WATHCDOG_LOG, level=logging.INFO)
        logging.info(datetime.now().time().strftime("%H:%M") + ' Start system')
        context = zmq.Context()
        self.socket = context.socket(zmq.PULL)
        self.socket.bind("tcp://*:" + self.cfg.WATCHDOG_SOCKET)

        while True:
            self.date_str = datetime.now().time().strftime("%H:%M")
            self.procmem()
            self.socket_recv()
            time.sleep(60)

    def check_state(self):
        flag_c = True
        flag_py = True
        flag_gpio = True
        flag_cam = True
        flag_leo = True
        flag_ttl = True
        if not self.flag_calibrate:
            while 0 in self.state_process:
                flag = False
                if self.state_process.index(0) == 0:
                    self.state_process[self.state_process.index(0)] = 1
                    logging.info(self.date_str + ' C++ code fall')
                    flag_c = False
                elif self.state_process.index(0) == 1:
                    self.state_process[self.state_process.index(0)] = 1
                    logging.info(self.date_str + ' interfaces code fall')
                    flag_py = False
                elif self.state_process.index(0) == 2:
                    self.state_process[self.state_process.index(0)] = 1
                    logging.info(self.date_str + ' GPIO code fall')
                    flag_gpio = False
            while 0 in self.state_periphery:
                if self.state_periphery.index(0) == 0:
                    self.state_periphery[self.state_periphery.index(0)] = 1
                    logging.info(self.date_str + ' cam fall')
                    flag_cam = False
                elif self.state_periphery.index(0) == 1:
                    self.state_periphery[self.state_periphery.index(0)] = 1
                    logging.info(self.date_str + ' leo fall')
                    flag_leo = False
                elif self.state_periphery.index(0) == 2:
                    self.state_periphery[self.state_periphery.index(0)] = 1
                    logging.info(self.date_str + ' ttl fall')
                    flag_ttl = False
            if not flag_c or not flag_py or not flag_gpio or not flag_cam:
                subprocess.check_call('/home/jetson/sauron/screens_script.sh  2', shell=True)
            elif not flag_leo:
                subprocess.check_call('/home/jetson/sauron/screens_script.sh  4', shell=True)
            elif not flag_ttl:
                subprocess.check_call('/home/jetson/sauron/screens_script.sh  3', shell=True)

    def by_to_Gby(self, count):
        return '{:.2f}'.format(float(count)/1024/1024/1024)

    def procmem(self):
        pypids = []
        depids = []
        for proc in psutil.process_iter(['pid', 'name', 'username']):
            if proc.info['username'] == 'jetson':
                if proc.info['name'] == 'python3':
                    pypids.append(proc.info['pid'])
                elif proc.info['name'] == 'trt_camera_inference':
                    depids.append(proc.info['pid'])
        if len(depids) != 0:
            c_swap = str(subprocess.check_output('cat /proc/' + str(depids[0]) + '/status |grep -i VmSwap', shell=True))[::-1]
            c_swap = int(c_swap.split(' ')[1][::-1])
        else:
            c_swap = 0.0
        if len(pypids) != 0:
            sub_py_swap_l = []
            for el in pypids:
                sub_py_swap = str(subprocess.check_output('cat /proc/' + str(el) + '/status |grep -i VmSwap', shell=True))[::-1]
                sub_py_swap_l.append(int(sub_py_swap.split(' ')[1][::-1]) * 1024)
            py_swap = max(sub_py_swap_l)
        else:
            py_swap = 0
        proc = psutil.cpu_percent(percpu=True)
        proc_aver = psutil.cpu_percent()
        mem = psutil.virtual_memory()
        swap = psutil.swap_memory()
        logging.info(self.date_str + ' proc 1: ' + str(proc[0]))
        logging.info(self.date_str + ' proc 2: ' + str(proc[1]))
        logging.info(self.date_str + ' proc 3: ' + str(proc[2]))
        logging.info(self.date_str + ' proc 4: ' + str(proc[3]))
        logging.info(self.date_str + ' proc_aver: ' + str(proc_aver))
        logging.info(self.date_str + ' used mem percent: ' + str(mem[2]) + '%')
        logging.info(self.date_str + ' used mem: ' + self.by_to_Gby(mem[3]) + ' Gb')
        logging.info(self.date_str + ' free mem: ' + self.by_to_Gby(mem[4]) + ' Gb')
        logging.info(self.date_str + ' used swap percent: ' + str(swap[3]) + '%')
        logging.info(self.date_str + ' used swap: ' + self.by_to_Gby(swap[1]) + ' Gb')
        logging.info(self.date_str + ' free swap: ' + self.by_to_Gby(swap[2]) + ' Gb')
        logging.info(self.date_str + ' used C swap: ' + '{:.2f}'.format(c_swap) +' Kb')
        logging.info(self.date_str + ' used Py swap: ' + '{:.2f}'.format(py_swap) + ' Kb')
        if self.max_proc['value'] == 0 or self.max_proc['value'] < float(proc_aver):
            self.max_proc['time'] = datetime.now().time().strftime("%H:%M")
            self.max_proc['value'] = float('{:.2f}'.format(proc_aver))
            logging.info(self.max_proc['time'] + ' MAX AVERAGE PROC: ' + str(self.max_proc['value']) + '%')
        if self.max_mem['value'] == 0 or self.max_mem['value'] < float(self.by_to_Gby(mem[3])):
            self.max_mem['time'] = datetime.now().time().strftime("%H:%M")
            self.max_mem['value'] = float(self.by_to_Gby(mem[3]))
            logging.info(self.max_mem['time'] + ' MAX MEMORY: ' + str(self.max_mem['value']) + ' Gb')
        if self.max_swap_mem['value'] == 0 or self.max_swap_mem['value'] < float(self.by_to_Gby(swap[3])):
            self.max_swap_mem['time'] = datetime.now().time().strftime("%H:%M")
            self.max_swap_mem['value'] = float(self.by_to_Gby(swap[3]))
            logging.info(self.max_swap_mem['time'] + ' MAX SWAP MEMORY: ' + str(self.max_swap_mem['value']) + ' Gb')

        if float(swap[3]) > 90:
            subprocess.check_call('sh -c /home/jetson/sauron/swap_reboot.sh', shell=True)

    def socket_recv(self):
        self.state_process = [0 for i in range(3)]
        self.state_periphery = [0 for i in range(3)]
        for i in range(24):
            try:
                mess = self.socket.recv_string(zmq.NOBLOCK)
            except:
                continue
            if mess.find('calibrate') != -1:
                self.flag_calibrate = True
            if not self.flag_calibrate:
                if mess.find('main_func') != -1:
                    self.state_process[1] = 1
                elif mess.find('gpio_func') != -1:
                    self.state_process[2] = 1
                elif mess.find('cam') != -1:
                    self.state_periphery[0] = 1
                elif mess.find('leo') != -1:
                    self.state_periphery[1] = 1
                elif mess.find('ttl') != -1:
                    self.state_periphery[2] = 1

        cipid = None
        for proc in psutil.process_iter(['pid', 'name', 'username']):
            if proc.info['username'] == 'jetson' and proc.info['name'] == 'trt_camera_inference':
                cipid =  proc.info['pid']
        if cipid is not None:
           self.state_process[0] = 1

        if not self.flag_calibrate:
            self.check_state()

if __name__ == '__main__':
    cntr = WatchDog()
