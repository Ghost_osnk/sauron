import fdb

# @author: barbass1025@gmail.com
# @link https://docs.google.com/document/d/1ooiODbX6zpno7lhP-9Y_0XLORbJ0hLe9NE7cre_YXg0/edit#heading=h.cn2j9t8rf01z
# Класс для работы с Smarket-системой (обмен списком товаров, категорий)
class SmarketServerApi:
    host = ''
    username = ''
    password = ''
    charset = ''
    connect = None
    cursor = None

    def __init__(self, host, username, password, charset):
        self.host = host
        self.username = username
        self.password = password
        self.charset = charset
        self.connection()

    # Подключение к базе данных
    def connection(self):
        self.connect = fdb.connect(dsn=self.host, user=self.username, password=self.password, charset=self.charset)
        # Объект курсора
        self.cursor = self.connect.cursor()

    # Получение категорий
    def get_category_list(self):
        self.cursor.execute("SELECT * FROM USR_SCALE_CATEGORY (null, null, null, 4)")

        category_list = []
        name_cat_list = []
        for row in self.cursor.fetchall():
            # Array ( [OUT_REZ] => 3 [OUT_BARCODE] => [OUT_ART] => [OUT_NAME] => [OUT_ID_CATEGORY] => 15 [OUT_NAME_CATEGORY] => БАНАНЫ )
            name = row[5].encode('cp1251').decode('cp1251')
            category = {
                'id': row[4],
                'name': name
            }
            name_cat_list.append(name)
            category_list.append(category)

        return category_list, name_cat_list

    # Получение товаров
    def get_product_list(self):
        self.cursor.execute("SELECT * FROM USR_SCALE_CATEGORY (null, null, null, 3)")

        product_list = []
        for row in self.cursor.fetchall():
            # Array ( [OUT_REZ] => 12 [OUT_BARCODE] => 2874090 [OUT_ART] => 01573 [OUT_NAME] => БАНАНЫ [OUT_ID_CATEGORY] => 15 [OUT_NAME_CATEGORY] => БАНАНЫ )
            name = row[3].encode('cp1251').decode('cp1251')
            product = {
                'id': row[2],
                'barcode': row[1],
                'vendor_code': row[2],
                'name': name
            }

            if row[4] != None:
                category_name = row[5].encode('cp1251').decode('cp1251')

                product['category'] = {
                    'id': row[4],
                    'name': category_name
                }

            product_list.append(product)

        return product_list

    # Создание категории
    def create_category(self, name):
        name_encode = name.encode('cp1251').decode('cp1251')
        self.cursor.execute("SELECT * FROM USR_SCALE_CATEGORY (null, null, ?, 1)", (name_encode,))
        self.cursor.fetchall()
        self.connect.commit()

    # Удаление категории
    def delete_category(self, category_id):
        self.cursor.execute("SELECT * FROM USR_SCALE_CATEGORY (null, ?, null, 5)", (category_id,))
        self.cursor.fetchall()
        self.connect.commit()

    # Привязка товара к категории
    def product_to_category(self, product_vendor_code, category_id):
        self.cursor.execute("SELECT * FROM USR_SCALE_CATEGORY (?, ?, null, 2)", (product_vendor_code, category_id,))
        self.cursor.fetchall()
        self.connect.commit()
