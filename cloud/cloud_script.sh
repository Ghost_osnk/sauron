#!/bin/bash
chmod 777 -R /home/jetson
pkill -9 python3
export DISPLAY=:0 && sudo -u jetson /usr/bin/python3 /home/jetson/sauron/screens.py 1 &
export HOME=/home/jetson && sudo -u jetson /usr/bin/python3 /home/jetson/sauron/cloud/cloud_uploader.py &
