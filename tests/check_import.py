import sys
sys.path.append('/home/jetson/sauron')
import os
import logging
from datetime import datetime, date, timedelta
from config import Config

cfg = Config()
logging.basicConfig(filename=os.path.join(cfg.TODAY_DIR, "log.log"), level=logging.INFO)
logging.info('--------------------')
logging.info(datetime.now().time().strftime("%H:%M:%S") + " Test system")
logging.info('--------------------')
flag = True

try:
    import tkinter as tk
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "tkinter": ' + str(e))
    flag = False

try:
    import smbus
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "smbus": ' + str(e))
    flag = False

try:
    import time
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "time": ' + str(e))
    flag = False

try:
    import os
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "os": ' + str(e))
    flag = False

try:
    from threading import Thread
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "Thread": ' + str(e))
    flag = False

try:
    import serial
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "serial": ' + str(e))
    flag = False

try:
    import shlex, subprocess
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "shlex, subprocess": ' + str(e))
    flag = False

try:
    import gc
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "gc": ' + str(e))
    flag = False

try:
    import glob
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "glob": ' + str(e))
    flag = False

try:
    import numpy as np
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "numpy": ' + str(e))
    flag = False

try:
    import random
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "random": ' + str(e))
    flag = False

try:
    import cv2
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "cv2": ' + str(e))
    flag = False

try:
    from PIL import *
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "PIL": ' + str(e))
    flag = False

try:
    import csv
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "csv": ' + str(e))
    flag = False

try:
    from subprocess import check_output
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error from "check_output": ' + str(e))
    flag = False

try:
    import re
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "re": ' + str(e))
    flag = False

try:
    import math
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Error import "math": ' + str(e))
    flag = False

if flag:
    logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Import unsudo libs successful')
else:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Import unsudo libs fail')
    subprocess.check_call('/usr/bin/python3 /home/jetson/sauron/screens.py 2', shell=True)