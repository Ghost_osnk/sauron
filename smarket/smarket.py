import sys
sys.path.append('/home/jetson/sauron')
import os
import configparser
import csv
import numpy as np
import logging
from datetime import datetime, date, timedelta

from config import Config
from revotail_server_api import RevotailServerApi
from smarket_server_api import SmarketServerApi

def csv_pars(name, name_classes):
    cat_dict = {}
    cat_list = []
    classes_list = []
    with open(name, encoding="utf-8") as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        spamreader = list(spamreader)
        for x, row in enumerate(spamreader):
            if x > 0:
                if x == 1:
                    cat_list.append(row[1])
                elif cat_list[-1] != row[1]:
                    cat_list.append(row[1])
                cat_dict[x] = {'product':row[0], 'category':row[1]}

    with open(name_classes, encoding="utf-8") as csvfile:
        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        spamreader = list(spamreader)
        for x, row in enumerate(spamreader):
            if row[0] != 'bg':
                classes_list.append(row[0])
    return cat_dict, cat_list, classes_list

def Search_el_on_dict(dict, name, searched):
    list_of_el = []
    for el in dict:
        if dict[el][name] == searched:
            list_of_el.append(el)
    return list_of_el

def Search_el_on_list(dict, name, searched):
    list_of_el = []
    for el in dict:
        if el[name] == searched:
            list_of_el.append(el)
    return list_of_el

def main():
    cfg = Config()
    logging.basicConfig(filename=os.path.join(cfg.TODAY_DIR, "log.log"), level=logging.INFO)
    logging.info('--------------------')
    logging.info(datetime.now().time().strftime("%H:%M:%S") + " Smarket sync")
    logging.info('--------------------')
    config_file = os.path.join(os.path.dirname((os.path.realpath(__file__))), 'config.local.ini')
    csv_file = os.path.join(cfg.DATABASE_DIR, 'smarket.csv')
    classes_file = os.path.join(cfg.DATABASE_DIR, 'NN_classes.csv')
    os.path.isfile(config_file)
    config = configparser.ConfigParser()
    config.read(config_file)

    new_csv_path = os.path.join(cfg.DATABASE_DIR, 'db.csv')
    delete_classes_path = os.path.join(cfg.DATABASE_DIR, 'delete_classes.csv')

    revotail_server_api = RevotailServerApi(config['REVOTAIL_SERVER_API']['URL'], config['REVOTAIL_SERVER_API']['DEVICE_ID'], config['REVOTAIL_SERVER_API']['PASSWORD'])
    try:
        smarket_server_api = SmarketServerApi(config['SMARKET_DATABASE']['HOST'], config['SMARKET_DATABASE']['USERNAME'], config['SMARKET_DATABASE']['PASSWORD'], config['SMARKET_DATABASE']['CHARSET'])
    except Exception as e:
        logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Initialization SmarketServerApi ERROR: ' + str(e))
    cat_dict, cat_list, classes_list = csv_pars(csv_file, classes_file)
    try:
        smarket_cat_dict, smarket_cat_list = smarket_server_api.get_category_list()
    except Exception as e:
        logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Get category before processing ERROR: ' + str(e))
    cat_list.sort()
    smarket_cat_list.sort()

    try:
        smarket_cat_dict, smarket_cat_list = smarket_server_api.get_category_list()
    except Exception as e:
        logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Get category list after processing ERROR: ' + str(e))

    try:
        smarket_product_list = smarket_server_api.get_product_list()
    except Exception as e:
        logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Get product ERROR: ' + str(e))

    for el_sm in smarket_product_list:
        keys = el_sm.keys()
        name_sm = el_sm['name']
        if 'category' in keys:
            cat_sm = el_sm['category']['name']
        else:
            cat_sm = ''            
        for el_revo in cat_dict:
            name_revo = cat_dict[el_revo]['product']
            if name_sm == name_revo:
                cat_revo = cat_dict[el_revo]['category']
                if cat_sm != cat_revo:
                    smarket_server_api.product_to_category(el_sm['vendor_code'], Search_el_on_list(smarket_cat_dict, 'name', cat_revo)[0]['id'])
    try:
        smarket_product_list = smarket_server_api.get_product_list()
    except Exception as e:
        logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Get product ERROR: ' + str(e))

    new_csv = new_csv_path
    f = open(new_csv, 'w')
    new_str = ''
    prev_code = ''
    i = 0
    flag = True
    for el in smarket_product_list:
        if 'category' in el.keys():
            flag = False
            if i == 0:
                new_str = el['barcode'] + ';' + el['name'] + ';' + el['category']['name'] + '\n'
                prev_code = el['vendor_code']
                i += 1
                flag = True
            elif prev_code != el['vendor_code']:
                new_str = el['barcode'] + ';' + el['name'] + ';' + el['category']['name'] + '\n'
                prev_code = el['vendor_code']
                flag = True
            if flag:
                f.write(new_str)
    f.close()

    actuality_cat = []
    for el in smarket_product_list:
        if 'category' in el.keys():
            actuality_cat.append(el['category']['name'])

    del_cat = []
    for el in classes_list:
        if el != 'bg':
            if not el in actuality_cat:
                del_cat.append(el)

    new_classes = delete_classes_path
    f = open(new_classes, 'w')
    for el in del_cat:
        f.write(el + '\n')
    f.close()
    try:
        revotail_server_api.save_category_list(smarket_cat_dict)
        revotail_server_api.save_product_list(product_list)
    except Exception as e:
        logging.error(datetime.now().time().strftime("%H:%M:%S") + ' error on revotail_server_api: ' + str(e))
if __name__ == '__main__':
    main()
