import os
from datetime import datetime, date, timedelta
from threading import Thread
import sys
sys.path.append('../')
sys.path.append('.')
sys.path.append('/home/jetson/sauron')
import boto3
from config import Config


class CloudUploader:
    def __init__(self):
        self.cfg = Config()
        self.now = date.today()
        self.curr_img_dir = self.cfg.TODAY_DIR
        self.log_filename = os.path.join(self.curr_img_dir, 'uploader_logfile.txt')
        self.log_file = open(self.log_filename, 'w')
        ses = boto3.session.Session()
        self.S3_BN = 'sauron.okey'
        self.s3 = ses.client(service_name='s3', endpoint_url='https://storage.yandexcloud.net')

    def upload_to_cloud(self, path, save_path):
        self.log_file.write(path + ': file uploading\n')
        try:
            self.s3.upload_file(path, self.S3_BN, save_path)
            self.log_file.write(str(datetime.now()) + ' finished uploading to ' + save_path)
            os.remove(path)
            self.log_file.write(str(datetime.now()) + ' deleted file\n')
        except Exception as ex:
            self.log_file.write(str(ex))

    def upload_all(self):
        self.log_file.write('STARTED UPLOAD ' + self.curr_img_dir + '\n')
        names = os.listdir(self.curr_img_dir)
        names = [k for k in names if not 'logfile' in k]
        for name in names:
            t_upl = Thread(target=self.upload_to_cloud, args=(
                os.path.join(self.curr_img_dir, name), '{:02d}{:02d}/{}'.format(self.now.day, self.now.month, name)))
            t_upl.start()
            t_upl.join()
        self.log_file.write(str(datetime.now()) + ' UPLOADED\n')
        self.log_file.flush()
        self.log_file.close()

        self.s3.upload_file(self.log_filename, self.S3_BN, '{:02d}{:02d}/{}'.format(self.now.day, self.now.month, self.log_filename.rsplit('/', 1)[-1]))
        os.remove(self.log_filename)
        os.rmdir(self.curr_img_dir)


up = CloudUploader()
up.upload_all()
