import shlex, subprocess
import tkinter as tk
import numpy as np
import logging
import serial
import smbus
import random
import time
import glob
import math
import csv
import cv2
import zmq
import os
import gc
import re
from datetime import datetime, date, timedelta
from subprocess import check_output
from PIL import ImageTk
from PIL import Image
from PIL import *
from threading import Thread
from NN.controller import Controller as Worker    
from config import Config

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)

    #init classes
        self.cfg = Config()
        self.sauron = Worker()
    #--init classes

#variables

    #logging
        logging.basicConfig(filename=os.path.join(self.cfg.TODAY_DIR, "log.log"), level=logging.INFO)
        logging.info(datetime.now().time().strftime("%H:%M:%S") + " Start system")
    #--logging

    #flags
        self.flag_product = False
        self.flag_predict = False
        self.flag_alphabet = False
        self.flag_start_predict = False
    #--flags

    #paths
        self.path_database = os.path.join(self.cfg.DATABASE_DIR, 'db.csv')
        self.path_img_product = self.cfg.PRODUCT_IMG_DIR
        self.path_img_category = self.cfg.CATEGORY_IMG_DIR
        self.path_img_sys = self.cfg.SYSTEM_IMG_DIR
        self.path_save_img = self.cfg.TODAY_DIR
        self.path_xml = self.cfg.XML
    #--paths

    #dicts & lists
        self.dict_prod = {}
        self.list_btns = []
        self.list_cur_top = []
        self.list_cur_per = []
        self.list_prev_top = []
        self.list_cur_product = []
        self.list_classes = self.cfg.CLASSES
        self.list_alphabet = self.cfg.ALPHABET
        self.list_delete_classes = self.cfg.DELETE_CLASSES
    #--dicts & lists

    #timers
        self.timer_not_bg_not_bg = 0
        self.timer_not_bg_bg = 0
        self.timer_bg_bg = 0
        self.threshold_timer_not_bg_not_bg = self.cfg.THRESHOLD_TIMER_NOT_BG_NOT_BG
        self.threshold_timer_not_bg_bg = self.cfg.THRESHOLD_TIMER_NOT_BG_BG
        self.threshold_timer_bg_bg = self.cfg.THRESHOLD_TIMER_BG_BG
        self.threshold_timer_scale = self.cfg.THRESHOLD_TIMER_SCALE
    #--timers

    #indexes pages
        self.A_screen_index = 0
        self.B_screen_index = 0
    #--indexes pages

    #interface
        self.icons_frame = tk.Frame(master, bg='white')
        self.icons_frame.pack(side='top', fill='both', expand='yes')
        self.bg = tk.Label(self.icons_frame, bg='white')
        self.bg.pack(side='top', fill='both', expand='yes')
    #--interface

    #i2c
        self.bus = smbus.SMBus(self.cfg.BUS_INDEX)
        self.slave_addr = self.cfg.SLAVE_ADRESS
    # --i2c

    # scale 
        self.baudrate = self.cfg.BAUDRATE
        self.momentum_w = -1
        self.ticks_fall_scale = self.cfg.TICKS_FALL_SCALE
    # --scale
    
    #other
        self.threshold_bg = self.cfg.THRESHOLD_BG
    #--other

    #watchdog
        context = zmq.Context()
        self.socket = context.socket(zmq.PUSH)
        self.socket.connect("tcp://localhost:" + self.cfg.WATCHDOG_SOCKET)
    #--watchdog
#--variables

#functions
        self.Init_scale()
        self.Parsing_csv()
        self.Calc_coord_size(master)
        self.Resize_imgs()
        self.Initialization_buttons()
#--functions

#threads
        thread_main_func = Thread(target=self.Main_func)
        thread_main_func.start()
        thread_reading_scale = Thread(target=self.Reading_scale)
        thread_reading_scale.start()
        thread_fall_devices = Thread(target=self.Check_periphery)
        thread_fall_devices.start()
        thread_check_scale = Thread(target=self.Check_scale)
        #thread_check_scale.start()
#--threads

#Initialization functions
    def Calc_coord_size(self, master):
        try:
            self.width_screen = master.winfo_screenwidth()
            self.height_screen = master.winfo_screenheight()

            self.space_width = int(self.width_screen/100*self.cfg.WIDTH_SPACE)
            self.space_height = int(self.height_screen/100*self.cfg.HEIGHT_SPACE)

            self.width_icons = int(self.width_screen/100*self.cfg.WIDTH_ICONS)
            self.height_icons = int(self.height_screen/100*self.cfg.HEIGHT_ICONS)
            self.y_init_icons = int(self.height_screen/100*self.cfg.Y_INIT_PRODUCTS)

            self.width_arrow = int(self.width_screen/100*self.cfg.WIDTH_ARROW)
            self.height_arrow = int(self.height_screen/100*self.cfg.HEIGHT_ARROW)

            self.x_left_arrow = int(self.width_screen/100*self.cfg.X_LEFT_ARROW)
            self.y_left_arrow = int((self.space_height + self.height_icons * 2)/2)

            self.height_icons_img = int(self.height_screen/100*self.cfg.HEIGHT_ICONS_IMG)

            self.height_offset = int(self.height_screen/100*self.cfg.HEIGHT_OFFSET)
            self.width_offset = int(self.width_screen/100*self.cfg.WIDTH_OFFSET)
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Calc_coord_size error: " + str(e))

    def Parsing_csv(self):
        try:
            product_img_list = os.listdir(self.path_img_product)
            category_img_list = os.listdir(self.path_img_product)
            with open(self.path_database, encoding="utf-8") as csvfile:
                spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
                spamreader = list(spamreader)
                for x, row in enumerate(spamreader):
                    self.dict_prod[x] = {'article':row[0], 'product':row[1].upper(), 'category':row[2].upper()}
                    if row[2] != 'НЕТ КАТЕГОРИИ':
                        if row[1].upper() + '.png' in product_img_list: 
                            sub_dict = {'img_product':Image.open(os.path.join(self.path_img_product, row[1].upper() + '.png'))}
                        else:
                            sub_dict = {'img_product':Image.open(os.path.join(self.path_img_product, 'НЕТ КАТЕГОРИИ.png'))}
                    elif row[2].upper() + '.png' in category_img_list:
                        sub_dict = {'img_product':Image.open(os.path.join(self.path_img_product, row[2].upper() + '.png'))}
                    else:
                        sub_dict = {'img_product':self.image_default_product}
                    self.dict_prod[x].update(sub_dict)
            for el in self.dict_prod:
                if self.Search_el_on_dict(self.dict_prod, 'category', self.dict_prod[el]['category'])!= None and len(self.Search_el_on_dict(self.dict_prod, 'category', self.dict_prod[el]['category'])) > 1:
                    try:
                        sub_dict = {'img_category':Image.open(os.path.join(self.path_img_category, self.dict_prod[el]['category'].upper() + '(группа).png'))}
                    except:
                        sub_dict = {'img_category':'1'}
                else:
                    sub_dict = {'img_category':None}
                self.dict_prod[el].update(sub_dict)
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Parsing_csv error: " + str(e))


    def Resize_imgs(self):
        try:
            self.image_default_product = Image.open(os.path.join(self.path_img_sys, 'default.png'))
            self.image_default_product = self.image_default_product.resize((int(self.width_icons), int(self.height_icons - self.height_icons_img)), Image.ANTIALIAS)
            self.image_default_product = ImageTk.PhotoImage(self.image_default_product)

            self.image_arrow_l = Image.open(os.path.join(self.path_img_sys, 'arrow_l.png'))
            self.image_arrow_l = self.image_arrow_l.resize((int(self.width_arrow), int(self.height_arrow)), Image.ANTIALIAS)
            self.image_arrow_l = ImageTk.PhotoImage(self.image_arrow_l)

            self.image_arrow_r = Image.open(os.path.join(self.path_img_sys, 'arrow_r.png'))
            self.image_arrow_r = self.image_arrow_r.resize((int(self.width_arrow), int(self.height_arrow)), Image.ANTIALIAS)
            self.image_arrow_r = ImageTk.PhotoImage(self.image_arrow_r)

            self.image_bg = Image.open(os.path.join(self.path_img_sys, 'bg.png'))
            self.image_bg = self.image_bg.resize((int(self.width_screen), int(self.height_screen)), Image.ANTIALIAS)
            self.image_bg = ImageTk.PhotoImage(self.image_bg)

            self.image_main_screen = Image.open(os.path.join(self.path_img_sys, 'main_screen.png'))
            self.image_main_screen = self.image_main_screen.resize((int(self.width_screen), int(self.height_screen)), Image.ANTIALIAS)
            self.image_main_screen = ImageTk.PhotoImage(self.image_main_screen)
   
            self.bg['image'] = self.image_main_screen

            for el in self.dict_prod:
                self.dict_prod[el]['img_product'] = self.dict_prod[el]['img_product'].resize((int(self.width_icons), int(self.height_icons - self.height_icons_img)), Image.ANTIALIAS)
                self.dict_prod[el]['img_product'] = ImageTk.PhotoImage(self.dict_prod[el]['img_product'])
                if self.dict_prod[el]['img_category'] is not None and self.dict_prod[el]['img_category'] != '1':
                    self.dict_prod[el]['img_category'] = self.dict_prod[el]['img_category'].resize((int(self.width_icons), int(self.height_icons - self.height_icons_img)), Image.ANTIALIAS)
                    self.dict_prod[el]['img_category'] = ImageTk.PhotoImage(self.dict_prod[el]['img_category'])
                elif self.dict_prod[el]['img_category'] == '1':
                    self.dict_prod[el]['img_category'] = self.image_default_product
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Resize_imgs error: " + str(e))

    def Initialization_buttons(self):
        try:
            self.arrow_l_b = tk.Button(self.bg,
                width=self.width_arrow,
                height=self.height_arrow,
                wraplength=150,
                image=self.image_arrow_l,
                compound="top",
                background="white",
                activebackground="white",
                highlightthickness = 0,
                borderwidth=0,
                relief=None,
                state='disabled')
            self.arrow_l_b.place(x=self.x_left_arrow, y=self.y_left_arrow)
            self.arrow_l_b['command'] = lambda self=self : self.Left_Button_Click()
            self.arrow_l_b.place_forget()

            for i in range (0, 6):
                button = tk.Button(self.bg,
                    width=self.width_icons - self.width_offset,
                    height=self.height_icons -self.height_offset,
                    text = '',
                    font='Arial 11 bold',
                    wraplength=150,
                    image=self.image_default_product,
                    compound="top",
                    justify="center",
                    background="white",
                    activebackground="white",
                    borderwidth=1,
                    relief='solid',
                )
                if 1<=i<=2:
                    button.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * i, y=self.y_init_icons)
                elif 3<=i<=5:
                    button.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * (i - 3), y=self.y_init_icons + (self.height_icons+ self.space_height))
                button.place_forget()
                self.list_btns.append(button)

            self.page = tk.Label(self.bg,
                width=int(self.width_screen/1000*5),
                height=int(self.height_screen/1000*3),
                background = 'white',
                font=("Arial", 30, "bold"), 
                fg='black',
                text='1' + '/' + str(int(math.ceil(len(self.list_alphabet)/6))),
                padx=0,
                pady=0,
                bd=0,
                borderwidth = 1,
                relief='solid',
                justify='right')
            self.page.place(x=self.width_screen - self.width_screen/100*12, y=self.height_screen - self.height_screen/100*12.3)
            self.page.place_forget()

            self.arrow_r_b = tk.Button(self.bg, 
                width=self.width_arrow,
                height=self.height_arrow,
                wraplength=150,
                image=self.image_arrow_r,
                compound="top",
                background="white",
                activebackground="white",
                highlightthickness = 0,
                borderwidth=0,
                relief=None)
            self.arrow_r_b.place(x=self.x_left_arrow + self.width_arrow + self.space_width * 4 + self.width_icons * 3, y=self.y_left_arrow)
            self.arrow_r_b['command'] = lambda self=self: self.Right_Button_Click()
            self.arrow_r_b.place_forget()
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Initialization_buttons error: " + str(e))
#--Initialization functions

#Sub function
    def Search_el_on_dict(self, dict, name, searched):
        try:
            list_of_el = []
            for el in dict:
                if dict[el][name] == searched:
                    list_of_el.append(el)
            return list_of_el
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Search_el_on_dict error: " + str(e))
#--Sub function

#Work with view button
    def Create_button(self, mode):
        try:
            if mode == '1':
                self.flag_predict = True
                self.bg['image']=self.image_bg
                for x, el in enumerate(self.list_btns):
                    if len(self.Search_el_on_dict(self.dict_prod, 'product', self.list_cur_top[x])) == 0:
                        if len(self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])) == 1:
                            el['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['product']
                            el['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['img_product']
                            el['bg'] = 'white'
                            el['activebackground'] = 'white'
                        else:
                            el['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['category']
                            el['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['img_category']
                            el['bg'] = '#C7D0D8'
                            el['activebackground'] = '#C7D0D8'
                    else:
                        el['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', self.list_cur_top[x])[0]]['product']
                        el['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', self.list_cur_top[x])[0]]['img_product']
                        el['bg'] = 'white'
                        el['activebackground'] = 'white'
                    el['fg'] = 'black'
                    el['command'] = lambda self=self, data=el['text'] : self.Button_Click(data=data)

                    if 0<=x<=2:
                        el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * x, y=self.y_init_icons)
                    elif 3<=x<=5:
                        el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * (x - 3), y=self.y_init_icons + (self.height_icons + self.space_height))

                self.arrow_r_b.place(x=self.x_left_arrow + self.width_arrow + self.space_width * 4 + self.width_icons * 3, y=self.y_left_arrow)

                self.arrow_l_b.place(x=self.x_left_arrow, y=self.y_left_arrow)
                self.page.place(x=self.width_screen - self.width_screen/100*12, y=self.height_screen - self.height_screen/100*12.3)
                self.page['text']='1/' + str(int(math.ceil(len(self.list_alphabet)/6)))
                self.timer_not_bg_not_bg = time.time()
            else:
                self.list_cur_product = self.Search_el_on_dict(self.dict_prod, 'category', mode)
                # print(self.list_cur_product)
                self.page['text'] = '1/' + str(int(math.ceil(len(self.list_cur_product)/6)))
                self.arrow_l_b['state'] = 'normal'
                for x, el in enumerate(self.list_btns):
                    if len(self.list_cur_product) > x + 6 * self.B_screen_index:
                        el['text'] = self.dict_prod[self.list_cur_product[x + 6 * self.B_screen_index]]['product']
                        el['image'] = self.dict_prod[self.list_cur_product[x + 6 * self.B_screen_index]]['img_product']
                        el['bg'] = 'white'
                        el['activebackground'] = 'white'
                        el['fg'] = 'black'
                        el['command'] = lambda self=self, data=self.list_btns[x]['text'] : self.Button_Click(data=data)
                        if el.winfo_ismapped() == 0:
                            if 0<=x<=2:
                                el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * x, y=self.y_init_icons)
                            elif 3<=x<=5:
                                el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * (x - 3), y=self.y_init_icons + (self.height_icons + self.space_height))
                if len(self.list_cur_product)<=6:
                    if len(self.list_cur_product)<6:
                        for i in range(len(self.list_cur_product), 6):
                            self.list_btns[i].place_forget()
                    self.arrow_r_b['state'] = 'disable'

                self.timer_not_bg_not_bg = time.time()
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Create_button error: " + str(e))

    def Change_button(self):
        try:
            self.timer_not_bg_not_bg = time.time()
            if self.flag_product:
                for x, el in enumerate(self.list_btns):
                    if len(self.list_cur_product) > x + 6 * self.B_screen_index:
                        el['text'] = self.dict_prod[self.list_cur_product[x + 6 * self.B_screen_index]]['product']
                        el['image'] = self.dict_prod[self.list_cur_product[x + 6 * self.B_screen_index]]['img_product']
                        el['bg'] = 'white'
                        el['activebackground'] = 'white'
                        el['fg'] = 'black'
                        el['command'] = lambda self=self, data=self.list_btns[x]['text'] : self.Button_Click(data=data)
                        if el.winfo_ismapped() == 0:
                            if 0<=x<=2:
                                el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * x, y=self.y_init_icons)
                            elif 3<=x<=5:
                                el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * (x - 3), y=self.y_init_icons + (self.height_icons + self.space_height))
                    else:
                        for x in range(len(self.list_cur_product) - self.B_screen_index * 6, 6):
                           self.list_btns[x].place_forget()

                self.timer_not_bg_not_bg = time.time()
            else:
                for x, el in enumerate(self.list_btns):
                    if len(self.list_alphabet) > x + 6 * self.A_screen_index:
                        if len(self.Search_el_on_dict(self.dict_prod, 'product', self.list_alphabet[x + 6 * self.A_screen_index])) == 0:
                            if len(self.Search_el_on_dict(self.dict_prod, 'category', self.list_alphabet[x + 6 * self.A_screen_index])) == 1:
                                el['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_alphabet[x + 6 * self.A_screen_index])[0]]['product']
                                el['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_alphabet[x + 6 * self.A_screen_index])[0]]['img_product']
                                el['bg'] = 'white'
                                el['activebackground'] = 'white'
                            else:
                                el['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_alphabet[x + 6 * self.A_screen_index])[0]]['category']
                                el['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_alphabet[x + 6 * self.A_screen_index])[0]]['img_category']
                                el['bg'] = '#C7D0D8'
                                el['activebackground'] = '#C7D0D8'
                        else:
                            el['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', self.list_alphabet[x + 6 * self.A_screen_index])[0]]['product']
                            el['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', self.list_alphabet[x + 6 * self.A_screen_index])[0]]['img_product']
                            el['bg'] = 'white'
                            el['activebackground'] = 'white'
                        el['fg'] = 'black'
                        el['command'] = lambda self=self, data=el['text'] : self.Button_Click(data=data)
                        if el.winfo_ismapped() == 0:
                            if 0<=x<=2:
                                el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * x, y=self.y_init_icons)
                            elif 3<=x<=5:
                                el.place(x=self.x_left_arrow + self.width_arrow + self.space_width + (self.width_icons + self.space_width) * (x - 3), y=self.y_init_icons + (self.height_icons + self.space_height))
                    else:
                        for x in range(len(self.list_alphabet) - self.A_screen_index * 6, 6):
                           self.list_btns[x].place_forget()
        except Exception as e:
            logging.warning(datetime.now().time().strftime("%H:%M:%S") + " Change_button error: " + str(e))
#--Work view with button

#Button clicks
    def Button_Click(self, data):
        print('Button_click()')
        try:
            if self.flag_product:
                try:
                    self.Send_code(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', data)[0]]['article'])
                except Exception as e:
                    logging.warning(datetime.now().time().strftime("%H:%M:%S") + " Button_Click error (438): " + str(e))
            else:
                if len(self.Search_el_on_dict(self.dict_prod, 'product', data)) == 0:
                    if len(self.Search_el_on_dict(self.dict_prod, 'category', data)) == 1:
                        try:
                            self.Send_code(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', data)[0]]['article'])
                        except:
                            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Button_Click error (447): " + str(e))
                    else:
                        self.flag_product = True
                        time.sleep(0.1)
                        self.Create_button(data)
                else:
                    try:
                        self.Send_code(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', data)[0]]['article'])
                    except:
                        logging.warning(datetime.now().time().strftime("%H:%M:%S") + " Button_Click error (456): " + str(e))
        except Exception as e:
            logging.warning(datetime.now().time().strftime("%H:%M:%S") + " Button_Click error: " + str(e))

    def Left_Button_Click(self):
        try:
            if self.flag_product:
                if self.B_screen_index > 0:
                    self.B_screen_index -= 1
                    self.page['text'] = str(self.B_screen_index + 1) + '/' + str(int(math.ceil(len(self.list_cur_product)/6)))
                    self.Change_button()
                else:
                    self.flag_product = False
                    self.page['text'] = str(self.A_screen_index + 1) + '/' + str(int(math.ceil(len(self.list_alphabet)/6)))
                    if self.A_screen_index != math.ceil(len(self.list_alphabet)/6) - 1:
                        self.arrow_r_b['state'] = 'normal'
                    if self.A_screen_index == 0:
                        self.Create_button('1')
                    else:
                        self.Change_button()
            else:
                if self.A_screen_index > 0:
                    self.page['text'] = str(self.A_screen_index) + '/' + str(int(math.ceil(len(self.list_alphabet)/6)))
                    if self.A_screen_index == 1:
                        self.flag_alphabet = False
                        self.Create_button('1')
                        self.A_screen_index -= 1
                        self.arrow_l_b['state']='disabled'
                    else:
                        self.arrow_r_b['state']='normal'
                        self.A_screen_index -= 1
                        self.Change_button()
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Left_Button_Click error: " + str(e))

    def Right_Button_Click(self):
        try:
            if self.flag_product:
                if self.B_screen_index < math.ceil(len(self.list_cur_product)/6) - 1:
                    self.B_screen_index += 1
                    self.page['text'] = str(self.B_screen_index + 1) + '/' + str(int(math.ceil(len(self.list_cur_product)/6)))
                    self.Change_button()
            else:
                if self.A_screen_index < math.ceil(len(self.list_alphabet)/6) - 1:
                    self.page['text'] = str(self.A_screen_index + 2) + '/' + str(int(math.ceil(len(self.list_alphabet)/6)))
                    if self.A_screen_index == 0:
                        self.flag_alphabet = True
                        self.arrow_l_b['state']='normal'
                    self.A_screen_index += 1
                    self.Change_button()
                    if self.A_screen_index == math.ceil(len(self.list_alphabet)/6) - 1:
                        self.arrow_r_b['state']='disabled'
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Right_Button_Click error: " + str(e))

#--Button clicks

#Periphery

    def leo_func(self):
        self.comp_leo = False
        self.flag_sub_leo = False
        leo = ''.join(os.popen('i2cdetect -y -r 0', 'r', 1).read().split('\n'))
        if leo.find('40: 40 -- -- -- -- -- -- -- -- -- -- -- -- -- -- --') != -1:
            self.flag_sub_leo = True
        self.comp_leo = True

    def Check_periphery(self):
        while True:
            try:
                self.socket.send_string('main_func')
                cam_flag = False
                scale_flag = False
                leo_flag = False
                scale_ticks = 0
                leo_ticks = 0
                if self.sauron.is_camera_online():
                    cam_flag = True
                while True:
                    if self.momentum_w == -1:
                        if scale_ticks == 0:
                            scale_ticks += 1
                        elif scale_ticks >= self.ticks_fall_scale:
                            scale_flag = False
                            break
                    else:
                        scale_flag = True
                        scale_ticks = 0
                        break
                    time.sleep(0.1)

                sub_leo_th = Thread(target=self.leo_func)
                sub_leo_th.start()
                while not self.comp_leo:
                    pass
                if self.flag_sub_leo:
                    leo_flag = True
                else:
                    leo_flag = False
                time.sleep(3)

                if cam_flag:
                    self.socket.send_string('cam')
                if leo_flag:
                    self.socket.send_string('leo')
                if scale_flag:
                    self.socket.send_string('ttl')
            except Exception as e:
                logging.error(datetime.now().time().strftime("%H:%M:%S") + " Check_periphery error: " + str(e))
            time.sleep(55)

    def Check_scale(self):
        while True:
            devices, ids = [x.replace('⎡', '').replace('↳ ', '').replace(' ', '') for x in list(map(lambda x: x[x.find(' '):x.find('        ')], ''.join(os.popen('xinput', 'r', 1).read()).split('\n')))], ''.join(os.popen('xinput list|grep id=|cut -f 2', 'r', 1).read()).split('\n')[:-1]
            bool_array = list(np.isin(self.cfg.DEVs, devices))
            if not np.array(bool_array).all():
                for el in bool_array:
                    if np.array(bool_array).all():
                        break
                    subprocess.Popen(shlex.split("xinput disable " + ids[bool_array.index(False)][3:]))
                    bool_array[bool_array.index(False)]=True
                    time.sleep(0.1)
            time.sleep(0.5)

    def Init_scale(self):
        flag = False
        os.chdir("/dev")
        try:
            for port in glob.glob("ttyUSB*"):
                if port is not None:
                    self.port = os.path.join('/dev', port)
                    self.ser = serial.Serial(port=self.port, baudrate=self.baudrate, timeout=3)
                    logging.info(datetime.now().time().strftime("%H:%M:%S") + " Initialization scale complete, port: " + str(self.port))
                    flag = True
                    break
                else:
                    flag = False
            if not flag:
                logging.error(datetime.now().time().strftime("%H:%M:%S") + " Initialization scale failed")
                subprocess.Popen("/usr/bin/python3 /home/jetson/sauron/screens.py 3",  shell=True)
        except:
            logging.info(datetime.now().time().strftime("%H:%M:%S") + " Initialization scale failed")

    def Reading_scale(self):
        self.timer_scale = 0
        self.flag_save = False
        while True:
            try:
                self.momentum_w = str(self.ser.read_until(b'\x00'))
            except Exception as e:
                self.momentum_w = -1
                continue
            if len(self.momentum_w) > 31 and self.momentum_w.find('x16gY') != -1:
                if not self.flag_start_predict:
                    self.flag_start_predict = True
                    self.timer_scale = time.time()
            else:
                continue  
            if time.time() - self.timer_scale > self.threshold_timer_not_bg_not_bg and not self.flag_save:
                sub_index_list = []
                self.timer_scale = 0
                self.flag_save = True
                for el in self.list_cur_top[0:6]:
                    if len(self.Search_el_on_dict(self.dict_prod, 'product', el)) > 1:
                        if len(self.Search_el_on_dict(self.dict_prod, 'category', el)) == 1:
                            sub_index_list.append(int(self.list_classes.index(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el)[0]]['category'])))
                        else:
                            sub_index_list.append(int(self.list_classes.index(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el)[0]]['product'])))
                    else:
                        sub_index_list.append(int(self.list_classes.index(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el)[0]]['category'])))
                self.sauron.save_img(str(sub_index_list))
                logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Save_img_scale')
                self.Return_to_main_screen()
                time.sleep(0.05)
            time.sleep(0.1)

    def Send_code(self, product):
        try:
            self.Save_image(product)
            print('product:',product)
            for x, sym in enumerate(product):
                self.bus.write_byte(self.slave_addr, ord(sym))
            self.bus.write_byte(self.slave_addr, ord('s'))
            self.Return_to_main_screen()
            time.sleep(0.05)
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Send_code error: " + str(e))

    def Save_image(self, product):
        try:
            self.flag_save = True
            self.timer_bg_s = time.time()
            sub_index_list = []
            for el in self.list_cur_top[0:6]:
                if len(self.Search_el_on_dict(self.dict_prod, 'product', el)) > 1:
                    if len(self.Search_el_on_dict(self.dict_prod, 'category', el)) == 1:
                        sub_index_list.append(int(self.list_classes.index(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el)[0]]['category'])))
                    else:
                        sub_index_list.append(int(self.list_classes.index(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el)[0]]['product'])))
                else:
                    sub_index_list.append(int(self.list_classes.index(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el)[0]]['category'])))
            if len(self.Search_el_on_dict(self.dict_prod, 'product', self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'article', product)[0]]['category'])) == 0:
                if len(self.Search_el_on_dict(self.dict_prod, 'category', self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'article', product)[0]]['category'])) == 0:
                    index = self.Search_el_on_dict(self.dict_prod, 'category', self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'article', product)[0]]['product'])[0]
                else:
                    index = self.Search_el_on_dict(self.dict_prod, 'category', self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'article', product)[0]]['category'])[0]
            else:
                index = self.Search_el_on_dict(self.dict_prod, 'category', self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'article', product)[0]]['product'])[0]
            self.sauron.save_img(str(index) + '_' + str(sub_index_list)) 
            logging.info(datetime.now().time().strftime("%H:%M:%S") + ' Save_img')
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + ' save_img error: ', str(e))
#--Periphery

#Work with predict
    def Difference_search(self):
        try:
            if not self.flag_alphabet and not self.flag_product:
                if self.list_cur_top[:5] != self.list_prev_top[:5]:
                    sub_arr = []
                    for el in self.list_btns:
                        if len(self.Search_el_on_dict(self.dict_prod, 'product', el['text'])) == 0:
                            if len(self.Search_el_on_dict(self.dict_prod, 'category', el['text'])) == 1:
                                if not self.flag_product and not self.flag_alphabet:
                                    sub_arr.append(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el['text'])[0]]['category'])
                            elif not self.flag_product and not self.flag_alphabet:
                                sub_arr.append(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', el['text'])[0]]['category'])
                        elif not self.flag_product and not self.flag_alphabet:
                            sub_arr.append(self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', el['text'])[0]]['category'])
                    bool_array = list(np.isin(sub_arr, self.list_cur_top[:6]))
                    if False in bool_array:
                        bool_array_1 = list(np.isin(self.list_cur_top[:6], sub_arr))
                        for x, el in enumerate(bool_array_1):
                            for y, el2 in enumerate(bool_array):
                                if not el:
                                    if not el2:
                                        if not self.flag_product and not self.flag_alphabet:
                                            if len(self.Search_el_on_dict(self.dict_prod, 'product', self.list_cur_top[x])) == 0:
                                                if len(self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])) == 1:
                                                    self.list_btns[y]['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['product']
                                                    self.list_btns[y]['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['img_product']
                                                    self.list_btns[y]['bg'] = 'white'
                                                    self.list_btns[y]['activebackground'] = 'white'
                                                else:
                                                    self.list_btns[y]['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['category']
                                                    self.list_btns[y]['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'category', self.list_cur_top[x])[0]]['img_category']
                                                    self.list_btns[y]['bg'] = '#C7D0D8'
                                                    self.list_btns[y]['activebackground'] = '#C7D0D8'
                                            else:
                                                self.list_btns[y]['text'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', self.list_cur_top[x])[0]]['product']
                                                self.list_btns[y]['image'] = self.dict_prod[self.Search_el_on_dict(self.dict_prod, 'product', self.list_cur_top[x])[0]]['img_product']
                                                self.list_btns[y]['bg'] = 'white'
                                                self.list_btns[y]['activebackground'] = 'white'
                                            self.list_btns[y]['command'] = lambda self=self, data=self.list_btns[y]['text'] : self.Button_Click(data=data)
                                            self.list_btns[y]['fg'] = 'black'
                                            bool_array[y]=True
                                        break
        except Exception as e:
            logging.warning(datetime.now().time().strftime("%H:%M:%S") + " Difference_search error: " + str(e))
#--Work with predict

#Change state
    def Return_to_main_screen(self):
        try:
            self.timer_not_bg_not_bg = 0
            self.B_screen_index = 0
            self.A_screen_index = 0
            self.flag_start_predict = False
            self.flag_predict = False
            self.flag_alphabet = False
            self.flag_product = False
            for el in self.list_btns:
                el.place_forget()
            self.arrow_l_b['state'] = 'disabled'
            self.arrow_r_b['state'] = 'normal'
            self.arrow_r_b.place_forget()
            self.arrow_l_b.place_forget()
            self.page.place_forget()
            self.timer_not_bg_bg = time.time()
            self.timer_bg_bg = time.time()
            self.bg['image'] = self.image_main_screen
            print('Return_to_main_screen()')
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Return_to_main_screen error: " + str(e))
#--Change state

    def Main_func(self):
        try:
            while True:
                gc.collect()
                if self.flag_start_predict:
                    if time.time() - self.timer_bg_bg > self.threshold_timer_bg_bg:
                        if self.flag_product or self.flag_alphabet:
                            if time.time() - self.timer_not_bg_not_bg > self.threshold_timer_not_bg_not_bg and self.timer_not_bg_not_bg != 0:
                                self.Return_to_main_screen()
                                time.sleep(0.05)

                        else:
                            if len(self.list_cur_top) != 0:
                                self.list_prev_top = self.list_cur_top
                            try:
                                get_top = self.sauron.get_top()
                            except Exception as e:
                                logging.warning(datetime.now().time().strftime("%H:%M:%S") + ' Get_top error: ' + str(e))
                                continue  
                            self.list_cur_top = get_top[0]
                            self.list_cur_per = get_top[1]
                            for el in self.list_delete_classes:
                                if el in self.list_cur_top:
                                    del self.list_cur_per[self.list_cur_top.index(el)]
                                    del self.list_cur_top[self.list_cur_top.index(el)]
                            if self.list_cur_top[0] == 'BG' and self.list_cur_per[0] < self.threshold_bg or self.list_cur_top[0] != 'BG':
                                if 'BG' in self.list_cur_top:
                                    del self.list_cur_per[self.list_cur_top.index('BG')]
                                    del self.list_cur_top[self.list_cur_top.index('BG')]
                                if self.flag_predict:
                                    if time.time() - self.timer_not_bg_not_bg > self.threshold_timer_not_bg_not_bg and self.timer_not_bg_not_bg != 0:
                                        self.Return_to_main_screen()
                                        time.sleep(0.05)
                                    elif not self.flag_product and not self.flag_alphabet:
                                        self.Difference_search()
                                else:
                                    if time.time() - self.timer_bg_bg > self.threshold_timer_bg_bg:
                                        self.timer_not_bg_not_bg = time.time()
                                        self.Create_button('1')
                            else:
                                if self.flag_predict:
                                    if time.time() - self.timer_not_bg_bg > self.threshold_timer_not_bg_bg and self.timer_not_bg_bg != 0:
                                        self.Return_to_main_screen()
                                        time.sleep(0.05)
                                    elif not self.flag_product and not self.flag_alphabet:
                                        self.Difference_search()
                time.sleep(0.1)
        except Exception as e:
            logging.error(datetime.now().time().strftime("%H:%M:%S") + " Main_func error: " + str(e))

try:
    root = tk.Tk()
except Exception as e:
    logging.error(datetime.now().time().strftime("%H:%M:%S") + ' Initialization Tkinter failed. Error text: ' + str(e))
root.wm_attributes('-fullscreen','true')
root.configure(bg="white")
app = Application(master=root)
app.master.title("Smarty касса")
app.mainloop()
