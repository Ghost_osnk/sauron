#!/bin/bash

sleep 7
export DISPLAY=:0 && sudo -u jetson /usr/bin/python3 /home/jetson/sauron/screens.py 0 &
chmod 777 /dev/ttyUSB*
chmod 777 /dev/video*
sudo -u jetson /usr/bin/python3 /home/jetson/sauron/tests/check_import.py
/usr/bin/python3 /home/jetson/sauron/tests/check_sudo_import.py
sudo -u jetson /usr/bin/python3 /home/jetson/sauron/tests/test_system.py
export DISPLAY=:0 && sudo -u jetson /usr/bin/python3 /home/jetson/sauron/wechsler.py &
export DISPLAY=:0 && /usr/bin/python3 /home/jetson/sauron/periphery/periphery_start_gpio.py 0 &
sleep 450 && export DISPLAY=:0 && sudo -u jetson /usr/bin/python3 /home/jetson/sauron/watchdog.py &
