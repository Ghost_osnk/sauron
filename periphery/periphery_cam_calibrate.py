import sys
sys.path.append('/home/jetson/sauron')
import os
import cv2
import time
import math
import numpy as np
import subprocess
import argparse
import json
import zmq
from threading import Thread
from config import Config
import glob

class Worker:

    def __init__(self):
        self.cfg = Config()
        self.refPt = []
        self.cropping = False
        self.clone = None
        
        context = zmq.Context()
        self.socket = context.socket(zmq.PAIR)
        self.socket.connect("tcp://localhost:2419")
        self.init_camera()
        self.calibr()

    def init_camera(self):
        for port in glob.glob("/dev/video*"):
            if port is not None:
                self.port = os.path.join('/dev', port)
                self.cap = cv2.VideoCapture(self.port)
                flag = True
                break
            else:
                flag = False
        if flag:
            self.cap.set(3, self.cfg.WH[0])
            self.cap.set(4, self.cfg.WH[1])
            self.cap.set(5, 30)
            self.cap.set(6, cv2.VideoWriter_fourcc(*'MJPG'))

    def select_area(self, event, x, y, flags, param):
        self.clone = self.frame.copy()
        if event == cv2.EVENT_LBUTTONDOWN:
            self.refPt = [(x, y)]
            self.cropping = False
        elif event == cv2.EVENT_MOUSEMOVE:
            refPt2 = [(x, y)]
            try:
                cv2.rectangle(self.clone, self.refPt[0], refPt2[0], (0, 255, 0), 2)
                cv2.imshow("calib img", self.clone)
            except:
                pass
        elif event == cv2.EVENT_LBUTTONUP:
            self.refPt.append((x, y))
            self.cropping = True
            cv2.rectangle(self.clone, self.refPt[0], self.refPt[1], (0, 255, 0), 2)
            cv2.imshow("calib img", self.clone)

    def calibr(self):
        cv2.namedWindow('calib img', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('calib img', 800, 600)
        self.t = Thread(target=self.inf_th)
        self.t.start()
        meta_dict = {'roi': None}
        cv2.namedWindow('calib img', cv2.WND_PROP_FULLSCREEN)
        cv2.setMouseCallback("calib img", self.select_area)
        cv2.setWindowProperty('calib img', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        _, self.frame = self.cap.read()
        (h,w) = self.frame.shape[:2]
        center = (w/2, h/2)
        while True:
            if self.clone is None:
                cv2.imshow('calib img', self.frame)
                cv2.putText(self.frame, 'Select region of interested', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
            else:
                cv2.imshow('calib img', self.clone)
                cv2.putText(self.clone, 'Select region of interested', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)    
            cv2.waitKey(1)

            if self.cropping:
                f = open(os.path.join(self.cfg.NN_DIR, 'meta_srn_sub.json'), 'w')
                list_f = []
                list_f.append(float(self.refPt[0][0]))
                list_f.append(float(self.refPt[0][1]))
                list_f.append(float(self.refPt[1][0]))
                list_f.append(float(self.refPt[1][1]))
                dict = {'roi': list_f}
                s = str(dict)
                s = s.replace("'", '"')
                f.write(s)
                f.close()
                self.refPt = []
                self.cropping = False

    def inf_th(self):
        while True:
            socket_mess = self.socket.recv().decode()
            if socket_mess == 'save':
                f = open(os.path.join(self.cfg.NN_DIR, 'meta_srn.json'), 'w')
                f1 = open(os.path.join(self.cfg.NN_DIR, 'meta_srn_sub.json'), 'r')
                for el in f1:
                    f.write(el)
                f.close()
                f1.close()
                self.socket.send_string('reboot')
                break
            elif socket_mess == 'reboot':
                if socket_mess == 'reboot':
                    self.socket.send_string('reboot')                
                break
            time.sleep(0.1)

if __name__ == '__main__':
    wrk = Worker()

